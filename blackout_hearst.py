'''Put slicer into Blackout during specified times and then rename asset to match _timestamp
'''


import urllib2, urllib, zlib, hmac, hashlib, time, json, datetime

ROOT_URL = 'http://services.uplynk.com'
OWNER = 'ACCOUNT GUID GOES HERE' # CHANGE THIS TO YOUR ACCOUNT GUID
SECRET = 'API SECRET GOES HERE' # CHANGE THIS TO YOUR SECRET API KEY

def Call(uri, **msg):
    msg['_owner'] = OWNER
    msg['_timestamp'] = int(time.time())
    msg = json.dumps(msg)
    msg = zlib.compress(msg, 9).encode('base64').strip()
    sig = hmac.new(SECRET, msg, hashlib.sha256).hexdigest()
    body = urllib.urlencode(dict(msg=msg, sig=sig))
    return json.loads(urllib2.urlopen(ROOT_URL + uri, body).read())


#Start and Stop times are in UTC. +4 hours to Eastern +7 to Pacific
from datetime import datetime
date = datetime.now().strftime('%Y-%m-%d')
start_1 = "08:30:00:00"
stop_1 = "11:00:00:00"
start_2 = "16:00:00:00"
stop_2 = "16:30:00:00"
start_3 = "20:00:00:00"
stop_3 = '22:30:00:00'
start_4 = "03:00:00:00"
stop_4 = "03:30:00:00"


#Update slicer_id to your live channel slicer ID.
Call('/api2/slicer/schedule/update', slicer_id='hearst_test', utc_offset=4, schedule=[
	{'date':date, 'timecode':start_1, 'break_type':'content_start', 'title':date + '_' + start_1},
	{'date':date, 'timecode':stop_1, 'break_type':'blackout'},
	{'date':date, 'timecode':start_2, 'break_type':'content_start', 'title':date + '_' + start_2},
	{'date':date, 'timecode':stop_2, 'break_type':'blackout'},
	{'date':date, 'timecode':start_3, 'break_type':'content_start', 'title':date + '_' + start_3},
	{'date':date, 'timecode':stop_3, 'break_type':'blackout'},
	{'date':date, 'timecode':start_4, 'break_type':'content_start', 'title':date + '_' + start_4},
	{'date':date, 'timecode':stop_4, 'break_type':'blackout'}])



#uncomment to get a local printout of the current slicer status during execution
#print Call('/api2/slicer/schedule/get', slicer_id='local_test')
